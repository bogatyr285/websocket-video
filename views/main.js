$(document).ready(function () {

    var socket = io('http://localhost:3000')

    //When we receive base64 image we draw it
    socket.on('stream', (image) => {
        var img = document.getElementById('player')
        img.src = image;
        //  var logger=document.getElementById('logger')
        //  $('#logger').text(image)
    })

    let canvas = document.getElementById('preview');
    var context = canvas.getContext('2d');

    canvas.width = 680;
    canvas.height = 320;
    context.width = canvas.width;
    context.height = canvas.height;

    var video = document.getElementById('video');

    function loadCam(stream) {
        video.src = window.URL.createObjectURL(stream)
    }
    function loadFail() {
        logger('Camera is not supported')
    }
    
    function streamVideo(video, context) {
        context.drawImage(video, 0, 0, context.width, context.height);
        // $('#logger').text(canvas.toDataURL());
        socket.emit('stream', canvas.toDataURL())
    }

    navigator.getUserMedia = (
        navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia ||
        navigator.msgGetUserMedia);

    if (navigator.getUserMedia) {
        navigator.getUserMedia({ video: true, audio: true }, loadCam, loadFail)
    }

    setInterval(() => {
        streamVideo( video, context);
    }, 125)
});


function logger(msg) {
    $('#logger').text(msg);
}
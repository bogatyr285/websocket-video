const Koa = require('koa');
const Router = require('koa-router');
const IO = require('koa-socket');
const Pug = require('koa-pug');

var app = new Koa();

var io = new IO();
io.attach(app);

var router = new Router();

var pug = new Pug({
    viewPath: './views',
    basedir: './views',
    app: app //Equivalent to app.use(pug)
});

router.get('/', (ctx, next) => {
    ctx.render('index')
});

io.on('connection', (ctx, data) => {
    console.log(`connected new client ${data}`)

    io.on('stream', (ctx, image) => {
        io.broadcast('stream',image)    
    })
    io.on('disconnect', (data) => {
        console.log(`client disconnected`)
    })
});

app.use(router.routes()); 
app.listen(3000,()=>{
    console.log('Server listening port 3000')
});